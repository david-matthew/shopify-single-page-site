/**
 * General JavaScript/jQuery functionality.
 */

$(document).ready(function() {

    /**
     * Initialize the Quietflow background animation.
     */
    $(".bg-img-full").quietflow({
        theme: "bouncingBalls",
        backgroundCol: "#ecf0f1",
        maxRadius: 80,
        bounceSpeed: 60,
        bounceBallCount: 25,
        transparent: true,
        specificColors: [
            "rgba(149, 191, 71, 0.35)",
            "rgba(149, 191, 71, 0.35)",
            "rgba(94, 142, 62, 0.35)", 
            "rgba(0, 0, 0, 0.2)"
        ]
    });
    
    /**
     * Change to vortex on click.
     */
    $('.vortex').click(function() {
        $(".bg-img-full").quietflow({
            theme : "vortex",
            mainRadius: 35,
            miniRadii: 25,
            backgroundCol: "#c9daa2",
            circleCol: "#ecf0f1",
            speed: 10
        })
    });

    /**
     * Change to bouncing balls on click.
     */
    $('.bouncing-balls').click(function() {
        $(".bg-img-full").quietflow({
            theme: "bouncingBalls",
            backgroundCol: "#ecf0f1",
            maxRadius: 80,
            bounceSpeed: 60,
            bounceBallCount: 25,
            transparent: true,
            specificColors: [
                "rgba(149, 191, 71, 0.35)",
                "rgba(149, 191, 71, 0.35)",
                "rgba(94, 142, 62, 0.35)", 
                "rgba(0, 0, 0, 0.2)"
            ]
        });
    });

    /**
     * Change to corner spikes on click.
     */
    $('.corner-spikes').click(function() {
        $(".bg-img-full").quietflow({
            theme: "cornerSpikes",
            backgroundCol: "#ecf0f1",
            specificColors: [
                "rgba(149, 191, 71, 0.35)",
                "rgba(94, 142, 62, 0.15)"
            ]
        });
    });

    /**
     * Change to a simple gradient on click.
     */
    $('.simple-gradient').click(function() {
        $(".bg-img-full").quietflow({
            theme : "simpleGradient",
            primary : "#efffe0",
            accent : "#ecf0f1"
        });
    });

    /**
     * Functionality for the navbar-toggle animations.
     */
    let menuIsHidden = true;
    $('#navbar-toggle').click(function() {
        if (menuIsHidden) {
            $('#menu-mobile').css('left', '10%');
            $('#navbar-toggle').css('transform', 'rotate(360deg)');
            $('#navbar-toggle div:nth-child(1)').css('transform', 'translateY(15px) rotate(-45deg)');
            $('#navbar-toggle div:nth-child(2)').css({'width':'0', 'opacity':'0'});
            $('#navbar-toggle div:nth-child(3)').css('transform', 'translateY(-15px) rotate(45deg)');
        } else {
            $('#menu-mobile').css('left', '100%');
            $('#navbar-toggle').css('transform', 'rotate(0deg)');
            $('#navbar-toggle div:nth-child(1)').css('transform', 'translateY(0) rotate(0deg)');
            $('#navbar-toggle div:nth-child(2)').css({'width':'80%', 'opacity':'1'});
            $('#navbar-toggle div:nth-child(3)').css('transform', 'translateY(0) rotate(0deg)');
        }

        menuIsHidden = !menuIsHidden;
    });

    /**
     * Functionality for toggling submenu items on mobile.
     */
    let submenus = document.querySelectorAll('.has-sub-menu');
    for (let i = 0; i < submenus.length; i++) {
        submenus[i].addEventListener('click', function() {
            submenus[i].classList.toggle('show');
        });
    }

    /**
     * Slider functionality.
     */
    const sliderExists = document.getElementById('slider');
    if (sliderExists) {
        const slider = tns({
            container: '#slider',
            items: 1,
            slideBy: 'page',
            navPosition: 'bottom',
            mouseDrag: true,
            autoplay: true,
            autoplayButtonOutput: false,
            autoplayTimeout: 7000,
            controls: false
        });
    }

    /**
     * Create show/hide functionality for the #great-fit section.
     */
    $('.reason-heading').click(function() {
        let reasonId = $(this).data('reason');
        $(this).find('.fas').toggleClass('flip-vertical')
        $('.reason-text[data-reason=' + reasonId + ']').slideToggle();
    });  

});
